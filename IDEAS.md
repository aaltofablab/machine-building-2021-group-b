# Ideas

1. Click back machine  
   You hit the button and then the machine hits it again.

2. Punch robot  
   You punch the robot and the robot punches you back.

3. Door keeper robot  
   A robot that would help to hold the door at the Fablab

## Final choice

Punch robot.

### Task distribution

Mechanism: Onni  
Actuation: Dinan  
Automation: Kris  

### Cycle 1

Until Monday:  
Dinan: KiCad design of the Stepper Shield for one motor.  
Onni: Mechanical design and custom parts to be made (3D printed, milled or other)  
Kris: Proof of concept interface that sends grbl via serial.  

