The Puncing Machine

This repo to be used to document the build of the machine. Use this readme file for documentation. The repository should include the following.

- Hero shot of the machine
- Bill of materials (BOM), code source and CAD files for custom parts
- Description on how to repeat the build
- Licence file

This machine is being built by the following students.

- Onni Eriksson
- Dinan Yan
- Krisjanis Rijnieks

## 3D-Model and files

The files are made with fusion 360 using parameters at all points, to have the ability to change the size of the holes and parts to fit the required rods and bearings. The idea is to 3D print these parts. The original stl files are included in machine-update.zip as well as fusion design files in case you want to use differen't size parts by changing the parameters.
I used nema 17 motor dimensions as help to design the model.
The stl files are located in the Machine.zip file, one of each is needed to print the whole machine. In addition the nema17 motor and controls is needed.

## Reguired parts

For making this project you need two rods with linear bearings with a diameter of 6mm and the bearing diameter to be 12mm. The length can be anywhere from 200mm to 1000mm.
You also need one 8mm rod without bearings with a diameter of 8mm and it needs to be around 100mm-150mm shorter than the other rods.
You need also two ball bearings with 6-10mm thickness 10mm inner diameter and 26mm outer diameter.
Lastly you will need a nema 17 motor and a control board for it.

## Motion Control Software

We use Arduino UNO with [Stepper Shield](https://github.com/jw4rd/stepper) by jw4rd and the [GRBL](https://github.com/gnea/grbl) CNC motion control library.

### Installation

1. Go to [GRBL](https://github.com/gnea/grbl) repository and download the latest version.
2. Extract it and copy the `grbl/grbl` folder into your Arduino libraries folder.
3. In Arduino IDE you should be able to access the `File/Examples/grbl/grblUpload` example.
4. Make sure the pin mappings are correct before upload.

The Stepper Shield uses different pin-out than in the GRBL library defaults. Also the `Dir` and `Step` pins have to be swapped. It is possible to re-map the pins in GRBL library. Locate the `cpu_map.h` file in the GRBL library folder. Look up the following code and apply the changes.

```
#define X_STEP_BIT        6
#define Y_STEP_BIT        4
#define Z_STEP_BIT        2
...
#define X_DIRECTION_BIT   7
#define Y_DIRECTION_BIT   5
#define Z_DIRECTION_BIT   3
```

Save the file go back to Arduino, open up the `grblUpload` example and uoload it to your Arduino UNO.

### Usage

You can use a general purpose G-Code sender such as the [Universal G-code Sender (UGS)](https://github.com/winder/Universal-G-Code-Sender) to test if it works. Of course you have to make sure that the phases of your stepper motor are connected correctly to the Stepper Shield. Read more about other software on the [GRBL Wiki](https://github.com/gnea/grbl/wiki/Using-Grbl) pages.

### Calculations

Units are defined in mm, but we need degrees. GRBL default values are in mm, the easiest way forward is to treat them as rotational values or degrees.

1. Motor steps for 1 deg has to be defined.
2. Feed rate can then be defined as degrees per minute.
3. To change the rotation, we pass values assuming that these are deg instead of mm.
